# Marketplace online-shop

<div align="center">
    <a href="https://www.php.net/releases/8.1/en.php" target="_blank">
        <img src="https://img.shields.io/badge/PHP-v8.1-777BB4?logo=PHP" alt="PHP version" title="PHP version">
    </a>
    <a href="https://laravel.com" target="_blank">
        <img src="https://img.shields.io/badge/Laravel Framework-v10-FF2D20?logo=Laravel" alt="Laravel version" title="Laravel version">
    </a>
</div>

## About

This is a web application used for merchants upload their products and clients can order the products and checkout the order and notify admin via email, also clients search for specific product and filter products by title and price.


## Development

Clone project locally and do the following steps:

1. Install **redis**, **mysql** v8, **composer** v2 and **php cli** v8.1
2. Navigate into the root of the project and copy `.env.example` as `.env`. Fill your local config in `.env` file.
3. Install dependencies with:
    ```shell script
    composer install
    ```
4. Run migrations:
    ```shell script
    php artisan migrate
    ```
5. Run seeder for fake data:
    ```shell script
    php artisan db:seed
    ```
6. Start development server:
    ```shell script
    php artisan serve
    ```
7. Run workers:
    ```shell script
    php artisan horizon
    ```
      
8. Now you can use this section that include a Postman collection for interacting with the project's API.  [Download Postman Collection](./postman_collection/collection_api.json)

## Test

### phpunit

Run unit tests with <a href="https://phpunit.de">phpunit</a>:

```shell script
./vendor/bin/phpunit --configuration ./phpunit.xml
```

### php code sniffer (phpcs)

Check code style with <a href="https://github.com/squizlabs/PHP_CodeSniffer">php code sniffer</a>:

```shell script
./vendor/bin/phpcs --standard=./phpcs.xml
```

php code sniffer comes with phpcbf to automatically correct coding standard violations.

To fix fixable(!) errors run:

```shell script
./vendor/bin/phpcbf --standard=./phpcs.xml
```

## Install script

install script will run all the necessary commands to run the project such as (composer, migration, seeder and create post-commit file for run unit test and code style after each commit):

```shell script
./install.sh
```

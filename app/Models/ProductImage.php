<?php

namespace App\Models;

use App\Helper\ImageHelper;
use App\Services\FileCacheService;
use Database\Factories\ProductImageFactory;
use Eloquent;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\ProductImage
 *
 * @property int $id
 * @property int $product_id
 * @property string $file_location
 * @property string $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Product $product
 * @method static ProductImageFactory factory($count = null, $state = [])
 * @method static Builder|ProductImage newModelQuery()
 * @method static Builder|ProductImage newQuery()
 * @method static Builder|ProductImage query()
 * @method static Builder|ProductImage whereCreatedAt($value)
 * @method static Builder|ProductImage whereFileLocation($value)
 * @method static Builder|ProductImage whereId($value)
 * @method static Builder|ProductImage whereProductId($value)
 * @method static Builder|ProductImage whereType($value)
 * @method static Builder|ProductImage whereUpdatedAt($value)
 * @mixin Eloquent
 */
class ProductImage extends Model
{
    use HasFactory;

    protected $table = 'product_images';

    protected $fillable = [
        'product_id',
        'type', // video or image type for bucket object storage
        'file_location'
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function getFileData(): ?string
    {
        if (is_null($this->file_location)) {
            throw new Exception("Failed to get file data. product_image id: #$this->id");
        }
        $data = FileCacheService::disk($this->type)->get($this->file_location);
        if (empty($data)) {
            throw new Exception("Failed to get image data. product_image id: #$this->id");
        }
        return $data;
    }

    /**
     * @throws Exception
     */
    public static function createAndStoreFakeImageInStorage(): string
    {
        $imageContent = ImageHelper::createFakeImage();

        $fileLocation = now()->toDateString() . '/' . uniqid() . '.png';

        Storage::disk('image')->put($fileLocation, $imageContent);

        return  $fileLocation;
    }
}

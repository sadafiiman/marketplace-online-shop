<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Carbon;

/**
 * App\Models\ProductCreateRequest
 *
 * @property int $id
 * @property int $user_id
 * @property string $owner_product_id
 * @property string $title
 * @property int $price
 * @property int $transport_cost
 * @property mixed|null $file
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ProductCreateRequest newModelQuery()
 * @method static Builder|ProductCreateRequest newQuery()
 * @method static Builder|ProductCreateRequest query()
 * @method static Builder|ProductCreateRequest whereCreatedAt($value)
 * @method static Builder|ProductCreateRequest whereFile($value)
 * @method static Builder|ProductCreateRequest whereId($value)
 * @method static Builder|ProductCreateRequest whereOwnerProductId($value)
 * @method static Builder|ProductCreateRequest wherePrice($value)
 * @method static Builder|ProductCreateRequest whereTitle($value)
 * @method static Builder|ProductCreateRequest whereTransportCost($value)
 * @method static Builder|ProductCreateRequest whereUpdatedAt($value)
 * @method static Builder|ProductCreateRequest whereUserId($value)
 * @mixin \Eloquent
 */
class ProductCreateRequest extends Model
{
    protected $fillable = [
        'owner_product_id', 'title', 'price', 'transport_cost', 'file'
    ];
}

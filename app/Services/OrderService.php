<?php

namespace App\Services;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use App\Notifications\OrderFinalizedNotification;
use App\Repositories\OrderRepository;
use App\Transformers\OrderItemTransformer;
use Illuminate\Support\Facades\Notification;

class OrderService
{
    private OrderRepository $orderRepository;

    /**
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function addToInPurchaseOrder(User $user, Product $product, int $quantity = 1): OrderItem
    {
        $order = $user->getInPurchaseOrder();
        return $this->orderRepository->addOrUpdateOrderItem($order, $product, $quantity);
    }

    public function getOrderItems(User $user)
    {
        return $this->orderRepository->getUserOrderItems($user);
    }

    public function removeItemFromOrder(OrderItem $orderItem): void
    {
        $this->orderRepository->removeItemFromOrder($orderItem);
    }

    public function getTotalPriceOrder(User $user): array
    {
        return $this->orderRepository->getTotalPriceOrder($user);
    }

    public function checkout(Order $order): void
    {
        $this->orderRepository->finalizeOrder($order);

        Notification::route('mail', strval(config('mail.admin_email')))
            ->notify(new OrderFinalizedNotification($order));

        // We can do more here like in case notify merchant from finalized order
        // another migration called ProductOrderToMerchant and added callback url from merchant transactional
    }

    public function updateTransportCost(Order $order, bool $orderTransportStatus): void
    {
        $this->orderRepository->updateTransportCost($order, $orderTransportStatus);
    }

    public function getResponseOrderDetail(User $user): array
    {
        $orderItems = $this->getOrderItems($user);
        $totalPrice = $this->getTotalPriceOrder($user);

        // Combine the order items data with has_transport and has_error key
        return [
            'has_error' => 0,
            'total_items_price' => $totalPrice['total_price'],
            'transport_cost' => $totalPrice['transport_cost'],
            'total_price' => $totalPrice['total_price'] + $totalPrice['transport_cost'],
            'data' => fractal($orderItems, new OrderItemTransformer())->toArray()['data'],
        ];
    }
}

<?php

namespace App\Services;

use App\Jobs\ProcessProductCreateRequestJob;
use App\Models\Product;
use App\Models\ProductCreateRequest;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use Throwable;

class ProductUploadService
{
    /**
     * @throws Throwable
     */
    public function uploadProduct(User $user, array $data): array
    {
        $productCreateReqs = [];
        /** @var array $response */
        $response =
            DB::transaction(function () use ($user, $data, &$productCreateReqs) {
                $user = User::query()->lockForUpdate()->find($user->id);
                if (is_null($user)) {
                    throw new Exception();
                }

                $response = [];
                foreach ($data as $d) {
                    $ownerProductIdInPCRsTable = ProductCreateRequest::query()
                        ->where('owner_product_id', '=', $d['owner_product_id'])
                        ->exists();

                    $ownerProductIdInProductsTable = Product::query()
                        ->where('user_id', '=', $user->id)
                        ->where('owner_product_id', '=', $d['owner_product_id'])
                        ->exists();

                    if ($ownerProductIdInProductsTable || $ownerProductIdInPCRsTable) {
                        $response[] = [
                            'owner_product_id' => $d['owner_product_id'],
                            'error_reason' => 'DUPLICATE_PRODUCT_ID'
                        ];
                        continue;
                    }

                    $content = base64_decode($d['image']);
                    if (mb_strlen($content, '8bit') > config('product.max_file_size')) {
                        $response[] = [
                            'owner_product_id' => $d['owner_product_id'],
                            'error_reason' => 'LARGE_FILE_SIZE',
                            'error_message' =>
                                'File size should be less than ' . config('product.max_file_size') . ' bytes'
                        ];
                        continue;
                    }
                    $productCreateRequest = ProductCreateRequest::query()->create([
                        'owner_product_id' => $d['owner_product_id'],
                        'title' => $d['title'],
                        'price' => $d['price'],
                        'transport_cost' => $d['transport_cost'],
                        'file' => $content,
                    ]);
                    $productCreateReqs[] = $productCreateRequest;
                    $response[] = [
                        'owner_product_id' => $d['owner_product_id'],
                        'has_error' => 0
                    ];
                }

                return $response;
            }, 3);

        foreach ($productCreateReqs as $pcr) {
            ProcessProductCreateRequestJob::dispatch($pcr);
        }

        return $response;
    }
}

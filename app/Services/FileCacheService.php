<?php

namespace App\Services;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\FilesystemException;
use Symfony\Component\HttpFoundation\StreamedResponse;

/** This service is to cache file locally.*/
class FileCacheService
{
    /**
     * @var string
     */
    private string $disk;

    /**
     * @param string $disk
     */
    public function __construct(string $disk)
    {
        $this->disk = $disk;
    }

    /**
     * @throws BindingResolutionException
     */
    public static function disk(string $disk): FileCacheService
    {
        /** @var FileCacheService $fileCacheService */
        $fileCacheService = app()->make(FileCacheService::class, ['disk' => $disk]);
        return $fileCacheService;
    }

    public function get(string $path): ?string
    {
        return $this->getFromCache($path) ?? $this->getWithoutUsingCache($path);
    }

    /**
     * @param string $path
     * @return StreamedResponse
     * @throws FilesystemException
     */
    public function download(string $path): StreamedResponse
    {
        return $this->downloadFromCache($path) ?? $this->downloadWithoutUsingCache($path);
    }

    private function getFromCache(string $path): ?string
    {
        $cachePath = $this->getCachePath($path);

        if (Storage::disk('file-cache')->has($cachePath)) {
            $content = Storage::disk('file-cache')->get($cachePath);
            if (!empty($content)) {
                return $content;
            } else {
                return null;
            }
        }
        return null;
    }

    private function getWithoutUsingCache(string $path): ?string
    {
        $this->putFileInCache($path);
        return Storage::disk($this->disk)->get($path);
    }

    /**
     * @param string $path
     * @return StreamedResponse|null
     * @throws FilesystemException
     */
    private function downloadFromCache(string $path): ?StreamedResponse
    {
        $cachePath = $this->getCachePath($path);

        if (Storage::disk('file-cache')->has($cachePath)) {
            return Storage::disk('file-cache')->download($cachePath);
        }
        return null;
    }

    /**
     * @param string $path
     * @return StreamedResponse
     */
    private function downloadWithoutUsingCache(string $path): StreamedResponse
    {
        $this->putFileInCache($path);
        return Storage::disk($this->disk)->download($path);
    }

    /**
     * @param string $path
     * @return string
     */
    private function getCachePath(string $path): string
    {
        return $this->disk . '/' . $path;
    }

    /**
     * @param string $path
     */
    private function putFileInCache(string $path): void
    {
        $cachePath = $this->getCachePath($path);

        Storage::disk('file-cache')->delete($cachePath);
        $content = Storage::disk($this->disk)->get($path);
        if (!empty($cachePath) && !empty($content)) {
            Storage::disk('file-cache')->put($cachePath, $content);
        }
    }

    public function deleteDirectory(string $path): bool
    {
        return Storage::disk('file-cache')->deleteDirectory($path);
    }
}

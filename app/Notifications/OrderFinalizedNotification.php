<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderFinalizedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected Order $order;

    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail($notifiable): MailMessage
    {
        $orderItems = $this->order->orderItems;

        $message = (new MailMessage())
            ->line('The order has been finalized.')
            ->line("user email: {$this->order->user->email}")
            ->line('Additional Information:')
            ->line('Order ID: #' . $this->order->id)
            ->line('Total Price: $' . number_format($this->order->total_price))
            ->line('Order Items:');

        foreach ($orderItems as $item) {
            $message->line(" - {$item->product->title} (Qty: {$item->quantity}, Price: {$item->product->price})");
        }

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}

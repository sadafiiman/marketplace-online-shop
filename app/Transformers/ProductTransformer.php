<?php

namespace App\Transformers;

use App\Helper\ImageHelper;
use App\Models\Product;
use App\Models\ProductImage;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    public function transform(Product $product): array
    {
        return [
            'id' => $product->id,
            'owner_product_id' => $product->owner_product_id,
            'title' => $product->title,
            'price' => $product->price,
            'transport_cost' => $product->transport_cost,
            'images' => $product->images->map(function (ProductImage $image) {
                return [
                  'id' => $image->id,
                  'image_content' => ImageHelper::pngToBase64Html($image?->getFileData())
                ];
            })->all()
        ];
    }
}

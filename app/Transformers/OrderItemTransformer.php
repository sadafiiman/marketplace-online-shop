<?php

namespace App\Transformers;

use App\Models\OrderItem;
use League\Fractal\TransformerAbstract;

class OrderItemTransformer extends TransformerAbstract
{
    public function transform(OrderItem $orderItem): array
    {
        return [
            'id' => $orderItem->id,
            'product_id' => $orderItem->product_id,
            'product_name' => $orderItem->product->title,
            'quantity' => $orderItem->quantity,
            'price' => $orderItem->product->price,
            'total_price' => $orderItem->total_price,
            'order_id' => $orderItem->order_id,
            'ordered_at' => $orderItem->updated_at->toDateTimeString(),
        ];
    }
}

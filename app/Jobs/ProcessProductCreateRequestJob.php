<?php

namespace App\Jobs;

use App\Models\Product;
use App\Models\ProductCreateRequest;
use App\Models\ProductImage;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProcessProductCreateRequestJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private ProductCreateRequest $productCreateRequest;

    /**
     * Create a new job instance.
     */
    public function __construct(ProductCreateRequest $productCreateRequest)
    {
        $this->productCreateRequest = $productCreateRequest;
        $this->onQueue('product::process-requests');
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        DB::transaction(function () {
            $now = Carbon::now();
            $ownerProductId = $this->productCreateRequest->owner_product_id;
            $userId = $this->productCreateRequest->user_id;
            $rawFile = strval($this->productCreateRequest->file);

            $fileName = $ownerProductId . '-' . $now->timestamp . '.png';
            $fileLocation = $userId . '/' . $now->toDateString() . '/' . $fileName;

            if (Storage::disk('image')->put($fileLocation, $rawFile) === false) {
                throw new Exception('Failed to write data');
            }

            $product = Product::query()->create([
                'user_id' => $userId,
                'owner_product_id' => $ownerProductId,
                'title' => $this->productCreateRequest->title,
                'price' => $this->productCreateRequest->price,
                'transport_cost' => $this->productCreateRequest->transport_cost,
            ]);

            ProductImage::query()->create([
                'product_id' => $product->id,
                'type' => 'image',
                'file_location' => $fileLocation,
            ]);
        }, 3);
    }
}

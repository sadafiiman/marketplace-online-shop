<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class SearchFilterFactory
{
    public static function create(string $filterType)
    {
        $filterClass = "App\\Filters\\" . ucfirst($filterType) . "Filter";

        if (class_exists($filterClass)) {
            return new $filterClass();
        }

        throw new \InvalidArgumentException("Filter type {$filterType} not supported.");
    }
}

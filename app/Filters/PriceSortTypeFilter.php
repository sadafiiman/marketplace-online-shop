<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class PriceSortTypeFilter implements SearchFilterInterface
{
    public function apply(Builder $query, $value): Builder
    {
        return $query->orderBy('price', $value);
    }
}

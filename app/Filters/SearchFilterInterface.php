<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

interface SearchFilterInterface
{
    public function apply(Builder $query, $value);
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Command\Command as CommandAlias;

class ClearFileCacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'file-cache:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear file cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $file = new Filesystem();
        $file->cleanDirectory(storage_path('app/file-cache'));
        return CommandAlias::SUCCESS;
    }
}

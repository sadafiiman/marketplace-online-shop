<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class UploadProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'data' => 'required|array|min:1|max:100',
            'data.*.title' => 'required|string',
            'data.*.owner_product_id' => 'required|string',
            'data.*.price' => 'nullable|numeric',
            'data.*.transport_cost' => 'required|numeric',
            'data.*.image' => 'required|string'
        ];
    }
}

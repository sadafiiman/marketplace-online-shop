<?php

namespace App\Http\Middleware;

use App\Http\Resources\UserResource;
use App\Models\Order;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckStatusOrderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Closure(Request): (Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $order = $request->route('order');

        if ($order->status != Order::STATUS_IN_PURCHASE) {
            return response()->json(
                new UserResource([
                    'has_error' => 1,
                    'data' => [
                        'message' => "Order: #$order->id is already finalized."
                    ]
                 ])
            );
        } elseif ($order->orderItems->isEmpty()) {
            return response()->json(
                new UserResource([
                    'has_error' => 1,
                    'data' => [
                        'message' => "Order: #$order->id has no Item."
                    ]
                ])
            );
        }
        return $next($request);
    }
}

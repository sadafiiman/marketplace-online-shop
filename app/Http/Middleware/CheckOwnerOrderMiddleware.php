<?php

namespace App\Http\Middleware;

use App\Http\Resources\UserResource;
use App\Models\Order;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckOwnerOrderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Closure(Request): (Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        /** @var User $user */
        $user = $request->user();

        /** @var Order $order */
        $order = $request->route('order');

        if ($order->user_id != $user->id) {
            return response()->json(
                new UserResource([
                    'has_error' => 1,
                    'data' => [
                        'message' => "You do not have permission to this order"
                    ]
                ]),
                403
            );
        }
        return $next($request);
    }
}

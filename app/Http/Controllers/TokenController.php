<?php

namespace App\Http\Controllers;

use App\Http\Requests\GenerateTokenApiRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TokenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user-api')->except(['generateToken']);
    }

    public function generateToken(GenerateTokenApiRequest $request): JsonResponse|UserResource
    {
        $validatedData = $request->validated();

        if (!Auth::attempt(['email' => $validatedData['email'], 'password' => $validatedData['password']])) {
            return response()->json(
                new UserResource([
                    'has_error' => 1,
                    'data' => [
                        'message' => 'Email or password is incorrect!'
                    ]
                ]),
                401
            );
        }

        /* @var UserResource $result */
        $result = DB::transaction(function () use ($validatedData) {
            $user = User::query()->where('email', $validatedData['email'])->lockForUpdate()->first();
            return new UserResource([
                'has_error' => 0,
                'data' => [
                    'token' => $user->createToken('user_' . $user->id . '_token', ['user-api'])->plainTextToken,
                    'message' => 'Token has been successfully generated.'
                ]
            ]);
        });

        return $result;
    }

    public function revokeAllToken(Request $request): UserResource
    {
        /** @var User $user * */
        $user = $request->user();

        /* @var UserResource $result */
        $result = DB::transaction(function () use ($user) {
            $user = User::query()->lockForUpdate()->findOrFail($user->id);
            if ($user->tokens()->count() == 0) {
                return new UserResource([
                    'has_error' => 1,
                    'data' => [
                        'message' => 'You do not have any tokens.'
                    ]
                ]);
            } else {
                $user->tokens()->delete();

                return new UserResource([
                    'has_error' => 0,
                    'data' => [
                        'message' => 'All tokens has been deleted.'
                    ]
                ]);
            }
        });

        return $result;
    }
}

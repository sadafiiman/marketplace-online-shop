<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Http\Resources\UserResource;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use App\Services\OrderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected OrderService $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
        $this->middleware('auth:user-api');
        $this->middleware(['check.order.owner', 'check.order.status'])
            ->only(['checkoutOrder', 'updateTransportStatus']);
    }

    public function addOrderItem(Request $request, Product $product): JsonResponse
    {
        $quantity = $request->input('quantity', 1);

        /* @var User $user */
        $user = $request->user();

        $this->orderService->addToInPurchaseOrder($user, $product, $quantity);

        $responseData = $this->orderService->getResponseOrderDetail($user);

        return response()->json($responseData);
    }

    public function getInvoiceOrder(Request $request): JsonResponse
    {
        /* @var User $user */
        $user = $request->user();

        $responseData = $this->orderService->getResponseOrderDetail($user);

        return response()->json($responseData);
    }

    public function removeItemFromOrder(Request $request, OrderItem $orderItem): JsonResponse
    {
        /* @var User $user */
        $user = $request->user();

        if ($orderItem->order->user_id != $user->id) {
            return response()->json(
                new UserResource([
                    'has_error' => 1,
                    'data' => [
                        'message' => "You do not have permission to this order"
                    ]
                ]),
                403
            );
        }

        $this->orderService->removeItemFromOrder($orderItem);
        $responseData = $this->orderService->getResponseOrderDetail($user);

        return response()->json($responseData);
    }

    public function updateTransportStatus(OrderRequest $request, Order $order): JsonResponse
    {
        $transportStatus = $request->validated();

        /* @var User $user */
        $user = $request->user();

        $this->orderService->updateTransportCost($order, $transportStatus['has_transport']);

        $responseData = $this->orderService->getResponseOrderDetail($user);

        return response()->json($responseData);
    }

    public function checkoutOrder(Order $order): UserResource
    {
        $this->orderService->checkout($order);

        return new UserResource([
            'has_error' => 0,
            'data' => [
                'message' => "Order: #$order->id has been finalized."
            ]
        ]);
    }
}

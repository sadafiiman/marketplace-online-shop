<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductSearchRequest;
use App\Http\Requests\UploadProductRequest;
use App\Http\Resources\UserResource;
use App\Models\Product;
use App\Models\User;
use App\Repositories\ProductRepository;
use App\Services\ProductUploadService;
use App\Transformers\ProductTransformer;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class ProductController extends Controller
{
    protected ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
        $this->middleware('auth:user-api')->except(['search']);
    }

    public function getAvailableList(Request $request): ?array
    {
        /* @var User $user */
        $user = $request->user();

        return fractal($user->availableProducts()->get(), new ProductTransformer())->toArray();
    }

    public function search(ProductSearchRequest $request): array
    {
        $filters = $request->validated();
        $products = $this->productRepository->search($filters);

        return fractal($products, new ProductTransformer())->toArray();
    }

    public function delete(Product $product): JsonResponse
    {
        try {
            $this->authorize('delete', $product);
            // soft delete the model
            $product->delete();
        } catch (AuthorizationException $exception) {
            return response()->json(
                new UserResource([
                    'has_error' => 1,
                    'data' => [
                        'message' => 'Product is not yours!'
                    ]
                ]),
                403
            );
        }

        return response()->json(
            new UserResource([
                'has_error' => 0,
                'data' => [
                    'message' => "Product: #$product->id has been successfully deleted."
                ]
            ])
        );
    }

    public function uploadProduct(Request $request, ProductUploadService $productUploadService)
    {
//        $data = (array)$request->validated()['data'];

        /** @var User $user */
        $user = $request->user();

        try {
            $response = $productUploadService->uploadProduct($user, $request->data);
            $response = new UserResource([
                'has_error' => 0,
                'data' => $response
            ]);
        } catch (Throwable $e) {
            $response = new UserResource([
                'has_error' => 0,
                'data' => ['message' => $e->getMessage()],
            ]);
        }

        return response()->json($response);
    }
}

<?php

namespace App\Helper;

use Exception;

class ImageHelper
{
    /**
     * @throws Exception
     */
    public static function createFakeImage(int $width = 100, int $height = 100): string
    {
        $img = imagecreatetruecolor($width, $height);
        if ($img === false) {
            throw new Exception();
        }

        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $color = intval(imagecolorallocate($img, random_int(0, 255), random_int(0, 255), random_int(0, 255)));
                imagesetpixel($img, $x, $y, $color);
            }
        }

        ob_start();
        imagepng($img);
        $result = ob_get_clean();
        if ($result === false) {
            throw new Exception();
        }
        return $result;
    }

    public static function pngToBase64Html(string $fileData)
    {
        return 'data:image/png;base64, ' . base64_encode($fileData);
    }
}

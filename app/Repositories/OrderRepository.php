<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;

class OrderRepository
{
    public function addOrUpdateOrderItem(Order $order, Product $product, int $quantity = 1)
    {
        $orderItem = $order->orderItems()->where('product_id', $product->id)->first();

        if ($orderItem) {
            $orderItem->update([
                'quantity' => $quantity,
                'total_price' => $product->price * $quantity
            ]);
        } else {
            $orderItem = OrderItem::query()->create([
                'order_id' => $order->id,
                'product_id' => $product->id,
                'quantity' => $quantity,
                'total_price' => $product->price * $quantity
            ]);
        }

        return $orderItem;
    }

    public function getUserOrderItems(User $user)
    {
        $order = $user->orders()
            ->where('status', Order::STATUS_IN_PURCHASE)
            ->first();

        return is_null($order) ? [] : $order->orderItems;
    }

    public function removeItemFromOrder(OrderItem $orderItem): void
    {
        $orderItem->delete();
    }

    public function updateOrderItemQuantity(OrderItem $orderItem, int $quantity): void
    {
        $orderItem->update(['quantity' => $quantity]);
    }

    public function getTotalPriceOrder(User $user): array
    {
        $order = $user->getInPurchaseOrder();

        $orderWithItems = Order::with('orderItems.product')->find($order->id);

        $totalPrice = 0;
        $totalTransportCost = 0;

        /* @var OrderItem $orderItem */
        foreach ($orderWithItems->orderItems as $orderItem) {
            $productPrice = $orderItem->product->price;
            $quantity = $orderItem->quantity;

            // Use the specific transport cost for each order item
            $transportCost = $order->has_transport ? $orderItem->product->transport_cost : 0;
            $totalTransportCost += $transportCost;
            // also we can calculate overlapped transport cost

            $totalPrice += ($productPrice * $quantity);
        }

        return [
            'total_price' => $totalPrice,
            'transport_cost' => $totalTransportCost
        ];
    }

    public function updateTransportCost(Order $order, bool $transportStatus): void
    {
        $order->update(['has_transport' => $transportStatus]);
    }

    public function finalizeOrder(Order $order): void
    {
        $totalPrice = $this->getTotalPriceOrder($order->user);
        $order->update([
            'status' => Order::STATUS_FINALIZED,
            'total_price' => ($totalPrice['total_price'] + $totalPrice['transport_cost'])
            ]);
    }
}

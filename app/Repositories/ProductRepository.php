<?php

namespace App\Repositories;

use App\Filters\SearchFilterFactory;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;

class ProductRepository
{
    protected Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function search($filters): Collection|array
    {
        $query = $this->product->newQuery()->NotDeleted();

        foreach ($filters as $filterType => $value) {
            $filter = SearchFilterFactory::create($filterType);
            $filter->apply($query, $value);
        }

        return $query->get();
    }
}

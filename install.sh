# Git pull
git pull origin main

# Install Composer dependencies
composer install

# Run database migrations and optimize
php artisan migrate

# Run database seeder
php artisan db:seed

# Run database seeder
php artisan optimize

# Create post-commit hook
echo "#!/bin/bash

# Redirect output to a log file
LOG_FILE=\".git/hooks/post-commit.log\"
./vendor/bin/phpcs --standard=./phpcs.xml > \"\$LOG_FILE\" 2>&1
./vendor/bin/phpunit --configuration ./phpunit.xml >> \"\$LOG_FILE\" 2>&1

# Display the contents of the log file in the terminal
cat \"\$LOG_FILE\"

# Exit with a non-zero status if either check fails
if [ \$? -ne 0 ]; then
   echo \"Post-commit checks failed. Please fix the issues before committing.\"
   exit 1
fi" > .git/hooks/post-commit

# Make post-commit hook executable
chmod +x .git/hooks/post-commit

#in case octane that is installed and already ran.
#php artisan octane:reload
php artisan serve

# Run horizon
#php artisan horizon
# Terminate Horizon (if horizon is installed and up)
#php artisan horizon:terminate

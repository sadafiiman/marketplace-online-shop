<?php

namespace Tests\Feature;

use App\Models\User;
use Faker\Generator;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\PersonalAccessToken;
use Tests\TestCase;

class SanctumTokenTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws BindingResolutionException
     */
    public function setUp(): void
    {
        parent::setUp();

        /* @var User $user */
        $user = User::factory()->createOne();

        $this->user = $user;

        /** @var Generator $faker */
        $faker = app()->make(Generator::class);
        $this->faker = $faker;

        $this->assertFalse(PersonalAccessToken::query()->where('tokenable_id', $this->user->id)->exists());
    }

    public function tearDown(): void
    {
        $directoryPath = now()->toDateString();
        Storage::disk('image')->deleteDirectory($directoryPath);
        parent::tearDown();
    }

    /**
     * @test
     */
    public function userShouldSuccessfullyGenerateToken(): void
    {
        $userData = [
            'email' => $this->user->email,
            'password' => 'password',
        ];

        $response = $this->postJson(route('api.token.generate'), $userData);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'has_error',
                'data' => [
                    'token',
                    'message',
                ],
            ]);

        $this->assertTrue(PersonalAccessToken::query()->where('tokenable_id', $this->user->id)->exists());

        $token = $response->json('data.token');
        $this->assertAuthenticatedAs($this->user, 'sanctum');
        $this->assertAuthenticated('sanctum');
    }

    /**
     * @test
     */
    public function userShouldNotSuccessfullyGenerateTokenBecauseOfWrongEmail(): void
    {
        $userData = [
            'email' => $this->faker->email,
            'password' => 'password',
        ];

        $response = $this->postJson(route('api.token.generate'), $userData);

        $response->assertStatus(401)
            ->assertJsonStructure([
                'has_error',
                'data' => [
                    'message',
                ],
            ]);

        $this->assertFalse(PersonalAccessToken::query()->where('tokenable_id', $this->user->id)->exists());

        $this->assertGuest('sanctum');
    }

    /**
     * @test
     */
    public function userShouldNotSuccessfullyGenerateTokenBecauseOfWrongPassword(): void
    {
        $userData = [
            'email' => $this->user->email,
            'password' => $this->faker->password,
        ];

        $response = $this->postJson(route('api.token.generate'), $userData);

        $response->assertStatus(401)
            ->assertJsonStructure([
                'has_error',
                'data' => [
                    'message',
                ],
            ]);

        $this->assertFalse(PersonalAccessToken::query()->where('tokenable_id', $this->user->id)->exists());

        $this->assertGuest('sanctum');
    }

    /**
     * @test
     */
    public function userShouldSuccessfullyRevokeAllTokens()
    {
        $token = $this->user->createToken('test_token')->plainTextToken;

        $response = $this->actingAs($this->user)->postJson(route('api.token.revoke'));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'has_error',
                'data' => [
                    'message',
                ],
            ]);

        $this->assertCount(0, $this->user->tokens);
    }

    /**
     * @test
     */
    public function userShouldNotSuccessfullyRevokeTokensBecauseUserDoesNotHaveAnyActiveTokens()
    {
        $response = $this->actingAs($this->user)->postJson(route('api.token.revoke'));

        $response->assertStatus(200)
            ->assertJson([
                'has_error' => 1,
                'data' => [
                    'message' => 'You do not have any tokens.',
                ],
            ]);

        $this->assertCount(0, $this->user->tokens);
    }
}

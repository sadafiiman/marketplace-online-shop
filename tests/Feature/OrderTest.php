<?php

namespace Tests\Feature;

use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use Faker\Generator;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws BindingResolutionException
     */
    public function setUp(): void
    {
        parent::setUp();

        $users = User::factory()->count(10)->create();

        $this->user = $users->random();

        $this->faker = app()->make(Generator::class);

        $this->product = Product::query()->inRandomOrder()->first();
    }

    public function tearDown(): void
    {
        $directoryPath = now()->toDateString();
        Storage::disk('image')->deleteDirectory($directoryPath);
        parent::tearDown();
    }

    /**
     * @test
     * @throws \Exception
     */
    public function userShouldSuccessfullyAddProductToOrderItems(): void
    {
        $randomQuantity = random_int(1, 5);
        $response = $this->actingAs($this->user, 'user-api')
            ->postJson(route('api.order.add', ['product' => $this->product->id]), [
            'quantity' => $randomQuantity,
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'has_error',
                'total_items_price',
                'transport_cost',
                'total_price',
                'data' => [
                    '*' => [
                        'id',
                        'product_id',
                        'product_name',
                        'quantity',
                        'price',
                        'total_price',
                        'order_id',
                        'ordered_at'
                    ],
                ],
            ]);

        $this->assertTrue(
            OrderItem::query()->where('order_id', $this->user->orders()->first()->id)
            ->where('product_id', $this->product->id)
            ->where('quantity', $randomQuantity)
            ->exists()
        );

        $this->assertDatabaseCount('order_items', 1);
    }
}

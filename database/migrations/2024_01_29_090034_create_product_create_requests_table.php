<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_create_requests', function (Blueprint $table) {
            //this table is for uploading data from merchants
                $table->id();
                $table->foreignId('user_id')->index()->constrained();
                $table->string('owner_product_id');
                $table->string('title');
                $table->unsignedBigInteger('price');
                $table->unsignedBigInteger('transport_cost');
                $table->binary('file');
                $table->timestamps();
        });
        if (DB::getDriverName() === 'mysql') {
            DB::statement("ALTER TABLE product_create_requests MODIFY COLUMN file MEDIUMBLOB");
        }

        // also we can define callback_url for each product when an order will finalize
        // call that callback url to tell merchant about the checkout order
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_create_requests');
    }
};

<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\ProductImage;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<User>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     * @throws Exception
     */
    public function definition(): array
    {
        $brand = $this->faker->randomElement(['Samsung', 'Apple', 'Huawei', 'Google', 'Xiaomi']);

        // Generate a mobile model based on the brand
        $modelPrefix = $this->getMobileModelPrefix($brand);
        $modelSuffix = $this->faker->randomLetter . $this->faker->randomNumber(2);

        return [
            'title' => "{$brand} {$modelPrefix} {$modelSuffix}",
            'owner_product_id' => uniqid(),
            'price' => $this->faker->numerify('#########'),
            'transport_cost' => $this->faker->numerify('######'),
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (Product $product) {
            ProductImage::factory()->for($product, 'product')->count(random_int(1, 20))->create();
        });
    }

    private function getMobileModelPrefix(string $brand)
    {
        return match ($brand) {
            'Samsung' => $this->faker->randomElement(['Galaxy', 'Note', 'A', 'S']),
            'Apple' => 'iPhone',
            'Huawei' => $this->faker->randomElement(['P', 'Mate', 'Nova']),
            'Google' => 'Pixel',
            'Xiaomi' => $this->faker->randomElement(['Mi', 'Redmi']),
            default => 'Unknown',
        };
    }
}

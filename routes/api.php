<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TokenController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::middleware('json.response')->group(function () {
    Route::prefix('token')->name('token.')->group(function () {
        Route::post('/generate', [TokenController::class, 'generateToken'])->name('generate');

        Route::post('/revoke-all', [TokenController::class, 'revokeAllToken'])->name('revoke');
    });

    Route::prefix('product')->name('product.')->group(function () {
        Route::post('/upload-data', [ProductController::class, 'uploadProduct'])->name('upload-data');
        Route::get('/search', [ProductController::class, 'search'])->name('search');

        Route::get('/my-available-list', [ProductController::class, 'getAvailableList'])
            ->name('available-list');

        Route::post('/delete/{product}', [ProductController::class, 'delete'])->name('delete');
    });

    Route::prefix('order')->name('order.')->group(function () {
        Route::get('/invoice/', [OrderController::class, 'getInvoiceOrder'])
            ->name('get-invoice-items');

        Route::post('/add/{product}', [OrderController::class, 'addOrderItem'])->name('add');

        Route::post('/remove-item/{orderItem}', [OrderController::class, 'removeItemFromOrder'])
            ->name('remove-item');

        Route::patch('/add-transport-cost/{order}', [OrderController::class, 'updateTransportStatus'])
            ->name('add-transport-cost');

        Route::post('/checkout/{order}', [OrderController::class, 'checkoutOrder'])
            ->name('checkout');
    });
});
